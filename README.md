# Spring Cloud Starter Config

Contains configuration for `java-microservices-starter-pack/spring-cloud-starter` microservice infrastructure:

`eureka-service.yml`  
`backend-service.yml`  
`frontend-service.yml`  
`api-gateway-service.yml`  